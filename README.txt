usage: elevator_cli.py [-h] elevator_cmds_file [{a,b}]

Elevator Problem. Program that requires an argument for the name of a
text file containing multiple sets of commands as an input, and for each
set of commands writes to standard output both the path that the elevator
will take and the total distance in floors that the elevator must travel.
In addition, the program accepts an argument to specify which mode the
elevator will operate in throughout the application lifecycle. The mode
argument should follow the filename argument. Ex.:
    $ ./elevator_cli.py elevator_test_input.txt A

positional arguments:
  elevator_cmds_file  The file containing elevator command set. Each line in the 
                      file is an entry. Ex. line:
                      init_floor: start-stop, start-stop, ..., start-stop
                      9: 1-5, 1-6, 1-5
  {a,b}               The elevator mode.
                      A or a - inefficient operation
                      B or b - efficient operation (default: b)

optional arguments:
  -h, --help          show this help message and exit


Examples of running:

    contenst of file elevator_test_input.txt:
        10:8-1
        9:1-5,1-6,1-5
        2:4-1,4-2,6-8
        3:7-9,3-7,5-8,7-11,11-1
        7:11-6,10-5,6-8,7-4,12-7,8-9
        6:1-8,6-8

    Calling A mode:
        ./elevator_cli.py elevator_test_input.txt A

        prints:
            10 8 1 (9)
            9 1 5 1 6 1 5 (30)
            2 4 1 4 2 6 8 (16)
            3 7 9 3 7 5 8 7 11 1 (36)
            7 11 6 10 5 6 8 7 4 12 7 8 9 (40)
            6 1 8 6 8 (16)

    Calling B mode:
        ./elevator_cli.py elevator_test_input.txt B

        prints:
            10 8 1 (9)
            9 1 5 6 (13)
            2 4 2 1 6 8 (12)
            3 5 7 8 9 11 1 (18)
            7 11 10 6 5 6 8 12 7 4 8 9 (30)
            6 1 6 8 (12)

#!/usr/bin/env python
'''

Elevator command line interface.

@author: Alex Volkov
@email: volkoa@gmail.com
'''
# ------------------------------------------------------ Python Standad Library
import sys
# import os
from argparse import (
    ArgumentDefaultsHelpFormatter, HelpFormatter, RawDescriptionHelpFormatter,
    ArgumentParser)
from textwrap import dedent

import traceback
# -------------------------------------------------------------- Custom modules
from elevator_lib import (parse_elev_cmdline, elevator_factory)

# ------------------------------------------------ module functions and Classes


class SmartFormatterMixin(HelpFormatter):
# ref: http://stackoverflow.com/questions/3853722/python-argparse-how-to-insert-newline-in-the-help-text  @IgnorePep8
    def _split_lines(self, text, width):
        # this is the RawTextHelpFormatter._split_lines
        if text.startswith('S|'):
            return text[2:].splitlines()
        return HelpFormatter._split_lines(self, text, width)


class CustomFormatter(ArgumentDefaultsHelpFormatter,
                      RawDescriptionHelpFormatter, SmartFormatterMixin):
    ''' Convenience formatter_class for argparse help print out.'''


def elevator_parser(desc):
    parser = ArgumentParser(description=dedent(desc),
                            formatter_class=CustomFormatter)
    parser.add_argument(
        'elevator_cmds_file', type=str,
        help='S|The file containing elevator command set. Each line in the \n'
        'file is an entry. Ex. line:\n'
        'init_floor: start-stop, start-stop, ..., start-stop\n'
        '9: 1-5, 1-6, 1-5')
    parser.add_argument(
        'elevator_mode', type=str.lower, nargs='?',
        choices=('a', 'b',),
        default='b',
        help='S|The elevator mode.\nA or a - inefficient operation\n'
        'B or b - efficient operation')

    args = parser.parse_args()

    return args


def main(argv=None):
    '''
    Elevator Problem. Program that requires an argument for the name of a
    text file containing multiple sets of commands as an input, and for each
    set of commands writes to standard output both the path that the elevator
    will take and the total distance in floors that the elevator must travel.
    In addition, the program accepts an argument to specify which mode the
    elevator will operate in throughout the application lifecycle. The mode
    argument should follow the filename argument. Ex.:
        $ ./elevator_cli.py elevator_test_input.txt A
    '''
    argv = sys.argv if argv is None else sys.argv.extend(argv)
    desc = main.__doc__

    # CLI parser
    args = elevator_parser(desc)

    elevator_cmds_file = args.elevator_cmds_file
    elevator_mode = args.elevator_mode

    # Get the proper elevator class for the chosen mode
    try:
        elevator_cls = elevator_factory(elevator_mode)
    except:
        traceback.print_exc()  # print traceback.

        # Exit the cli with exception.
        raise Exception('Could not instantiate elevator mode: "{}"'.format(
            elevator_mode))

    elev_inst = elevator_cls()

    # Process the file with elevator commands
    with open(elevator_cmds_file) as ef:
        # read line-by-line
        for ln in ef:
            cmdline = ln.strip()
            if not cmdline:
                continue

            # Parse command line
            try:
                init_flr, flrs_list = parse_elev_cmdline(cmdline)
            except:
                print >> sys.stderr, \
                    'Could not parse elevator command line: {}'.format(cmdline)
                traceback.print_exc()
                continue

            # Run the elevator and print answer to stdout
            elev_inst.run(init_flr, flrs_list)


if __name__ == '__main__':
    main()

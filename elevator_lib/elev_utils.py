'''

Elevator convenience utilities.

'''
from .elev_modes import (ElevatorA, ElevatorB)

__all__ = ('parse_elev_cmdline', 'elevator_factory',)

# ------------------------------------------------ module functions and Classes


def parse_elev_cmdline(cmdline):
    '''Parses the cmdline.

    :param cmdline: str. Commandline with the following format:
        "init_floor: start-stop, start-stop, ..., start-stop"
        Ex.: "9: 1-5, 1-6, 1-5"

    :returns: Initial Floor and list of lists for floors.
        (9, [[1, 5], [1, 6], [1, 5]])
    '''
    cmd_split = cmdline.split(':', 1)
    init_flr = int(cmd_split[0])

    # Nested list expressions
    flrs = [
        [int(start), int(stop)] for start, stop in
        [flr_pairs.strip().split('-') for flr_pairs in cmd_split[1].split(',')]
    ]

    return init_flr, flrs


def elevator_factory(elevator_mode):
    '''Returns and an elevator class according to IElevator adapter.

    :param elevator_mode: str. Case insensitive choice for elevator mode.
        'A' or 'B'.

    :returns: An elevator class. Currently either ElevatorA() or ElevatorB()
    :rtype: IElevator

    '''
    try:
        elevator_cls = {'a': ElevatorA, 'b': ElevatorB}[elevator_mode]
        return elevator_cls
    except KeyError:
        raise Exception('Unknown elevator mode: "{}"'.format(elevator_mode))

'''
Utilities for capturing and manipulating sysio.
'''

import sys
import contextlib

__all__ = ('capture',)


@contextlib.contextmanager
def capture():
    ''' Use to capture stdout and stderr to out[0] and out[1] respectively.
    :Example:

        with capture() as out:
            # everything is captured in out and nothing gets actually printed
            do whatever and print 'some message'
            print >>sys.stderr, 'some error messag'
        # now print out if desired or handle however needed
        print >> sys.stdout, out[0]
        print >> sys.stderr, out[1]

    '''
    from cStringIO import StringIO
    oldout, olderr = sys.stdout, sys.stderr
    out = [StringIO(), StringIO()]
    sys.stdout, sys.stderr = out
    try:
        yield out
    except Exception as err:
        raise(err)
    finally:
        sys.stdout, sys.stderr = oldout, olderr
        out[0] = out[0].getvalue()
        out[1] = out[1].getvalue()

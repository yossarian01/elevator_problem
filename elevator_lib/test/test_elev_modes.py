#!/usr/bin/env python
'''
Test the elevator modes.
'''
import unittest

from .sysio_utils import capture


from elevator_lib import (ElevatorA, ElevatorB)


class TestElevModes(unittest.TestCase):
    def test_elev_a(self):
        print '\n\n------------------------ test_elev_a --------------------\n'
        elev_inst = ElevatorA()
        init_flr, flrs_list = (9, [[1, 5], [1, 6], [1, 5]])

        with capture() as out:
            elev_inst.run(init_flr, flrs_list)

        result = out[0]
        print 'Ran elevator mode A for init_flr, flrs_list: {}'.format(
            (init_flr, flrs_list))
        print 'Result: {}'.format(result)
        assert result in ('9 1 5 1 6 1 5 (30)\n',)

        if out[1]:
            print 'Errors: ', out[1]

    def test_elev_b(self):
        print '\n\n------------------------ test_elev_b --------------------\n'
        elev_inst = ElevatorB()
        init_flr, flrs_list = (9, [[1, 5], [1, 6], [1, 5]])

        with capture() as out:
            elev_inst.run(init_flr, flrs_list)

        result = out[0]
        print 'Ran elevator mode A for init_flr, flrs_list: {}'.format(
            (init_flr, flrs_list))
        print 'Result: {}'.format(result)
        assert result in ('9 1 5 6 (13)\n',)

        if out[1]:
            print 'Errors: ', out[1]


# To test run:
#     $ python -m unittest discover
# or $ python -m elevator_lib.test.test_elev_modes
if __name__ == '__main__':

    unittest.main()

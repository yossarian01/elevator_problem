#!/usr/bin/env python
'''
Test the elevator miscellaneous utilities.
'''
import unittest

from elevator_lib import (parse_elev_cmdline, elevator_factory)


class TestElevUtils(unittest.TestCase):

    def test_parse_elev_cmdline(self):

        print '\n\n------------------ test_parse_elev_cmdline --------------\n'

        cmd_str = '9: 1-5, 1-6, 1-5'
        result = parse_elev_cmdline(cmd_str)

        print 'Parsed cmdline:\n    {}'.format(cmd_str)
        print 'Result: {}'.format(result)
        assert result in ((9, [[1, 5], [1, 6], [1, 5]]),)

    def test_elev_factory(self):
        print '\n\n---------------------- test_elev_factory ----------------\n'
        elev_modes = ('a', 'b', 'Z',)
        # Z doesn't exist
        for em in elev_modes:
            if em in ('Z',):
                try:
                    _ = elevator_factory(em)
                except Exception as err:
                    print 'Exception: {}'.format(err)
                    print 'Correctly threw exception for {} mode.\n'.format(em)

            else:
                _ = elevator_factory(em)
                print 'Successfully created elevator mode {}.\n'.format(em)


# To test run:
#     $ python -m unittest discover
# or python -m elevator_lib.test.test_elev_utils
if __name__ == '__main__':

    unittest.main()

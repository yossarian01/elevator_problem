'''
Created on Aug 21, 2015

@author: volkale

Desc.:
    To test run:
        $ python -m unittest discover

    or for a specific test such as test_some_module:
        $ python -m somepy _lib.test.test_some_module

'''
# ref: https://docs.python.org/2/library/unittest.html
# ref: http://stackoverflow.com/questions/7685483/optimal-file-structure-organization-of-python-module-unittests  @IgnorePep8
# Run test modules directly as scripts using the -m option, or using nose.
# To test run:
#     $ python -m unittest discover
#
# or specific module test:
#    python -m somepy_lib.test.test_some_module

#    nosetests somepy_lib /test/test_some_module.py

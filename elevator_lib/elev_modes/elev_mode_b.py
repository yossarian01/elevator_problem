'''
Efficient elevator. Mode B allows for operating the elevator more
efficiently. Similar to Mode A, the elevator in Mode B will acknowledge
each command in the order it is received. However Mode B supports
transporting more people at once and if any consecutive requests to travel
in the same direction are received, the elevator can handle them all in one
trip. If in the example above Doug and Alan had requested the elevator in
Mode B, the elevator would have picked up Doug on 4, picked up Alan on 3,
dropped off Alan on 2, and finally dropped off Doug on 1. In other words,
the commands 4-1, 3-2 can be merged because they are consecutive requests
to travel from a higher floor to a lower floor.
'''

from .elev_base import IElevator


__all__ = ('ElevatorB',)

# ------------------------------------------------ module functions and Classes


class ElevatorB(IElevator):
    '''
    Efficient elevator. Mode B allows for operating the elevator more
    efficiently. Similar to Mode A, the elevator in Mode B will acknowledge
    each command in the order it is received. However Mode B supports
    transporting more people at once and if any consecutive requests to travel
    in the same direction are received, the elevator can handle them all in one
    trip. If in the example above Doug and Alan had requested the elevator in
    Mode B, the elevator would have picked up Doug on 4, picked up Alan on 3,
    dropped off Alan on 2, and finally dropped off Doug on 1. In other words,
    the commands 4-1, 3-2 can be merged because they are consecutive requests
    to travel from a higher floor to a lower floor.
    '''
    # def __init__(self, *args, **kwargs):
    #     '''Nothing to init '''

    def _unload_passengers(self, cur_dir, last_get_off_flr, elev_queue):
        '''Return a list of passengers as they would be unloaded from the
        elevator.

        :param cur_dir: bool. True is up, False is down. Direction the
            elevator is traveling in.
        :param last_get_off_flr: int. The final floor from the last elevator
            trip.
        :param elev_queue: list. List of floors queued up.

        :returns: list. A flat list of floors optimized for the current
            elevator trip.
        '''
        # unload the passengers
        if cur_dir:
            # up case
            flat_list = sorted(set(sum(elev_queue, [])))
        else:
            # down case
            flat_list = sorted(set(sum(elev_queue, [])), reverse=True)

        # special case where last get_off floor is same as in
        # first in sorted unique elev_queue.
        if flat_list[0] == last_get_off_flr:
            flat_list.pop(0)

        return flat_list

    def run(self, init_flr, flrs_list):
        '''Print to standard output a single line consisting of a
        space-delimited list of floors followed by the total distance in floors
        that the elevator travels, in parenthesis "(" and ")".

        :Example:

            init_flr = 9; flrs_list = [[1, 5], [1, 6], [1, 5]]
            prints out: 9 1 5 6 (13)

        :param init_flr: int. Initial floor
        :param flrs_list: list of list of ints. The floors to traverse.

        :returns: None. Prints out the list of floors and distance.

        '''
        # True is up, False is down
        cur_dir = flrs_list[0][1] > flrs_list[0][0]
        prev_flr = init_flr
        elev_queue = [flrs_list[0]]
        dist = 0
        get_off = [prev_flr]

        for flr in flrs_list[1:]:
            new_dir = flr[1] > flr[0]
            if new_dir == cur_dir:
                elev_queue += [flr]
            else:
                flat_list = self._unload_passengers(
                    cur_dir, get_off[-1], elev_queue)
                get_off += flat_list

                # Start a new elevator queue.
                elev_queue = [flr]

                # Note the new elevator direction.
                cur_dir = new_dir

        flat_list = self._unload_passengers(cur_dir, get_off[-1], elev_queue)
        get_off += flat_list

        # PRINT THE ANSWER
        for flr_off in get_off:
            dist += abs(flr_off - prev_flr)
            prev_flr = flr_off
            print flr_off,

        print '({})'.format(dist)

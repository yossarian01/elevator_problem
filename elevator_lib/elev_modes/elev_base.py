'''

Elevator interfaces and base classes.
'''
# ------------------------------------------------------ Python Standad Library
import abc


__all__ = ('IElevator',)

# ------------------------------------------------ module functions and Classes


class IElevator(object):
    __metaclass__ = abc.ABCMeta

    # def __init__(self, *args, **kwargs):
    #     '''
    #     '''

    @abc.abstractmethod
    def run(self, init_flr, flrs_list):
        '''Run the elevator.

        :param init_flr: int. Initial floor
        :param flrs_list: list of list of ints. The floors to traverse.
        '''
        pass

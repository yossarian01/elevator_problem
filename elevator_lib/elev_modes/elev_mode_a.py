'''
Inefficient elevator. Mode A is very inefficient and only allows for
transporting one person at a time. If Doug calls the elevator on floor 4
and wants to go to floor 1, and Alan subsequently calls the elevator on
floor 3 and wants to go to floor 2, the elevator will go pick up Doug and
take him to floor 1 before picking up Alan and taking him to floor 2.
'''

from .elev_base import IElevator

__all__ = ('ElevatorA',)

# ------------------------------------------------ module functions and Classes


class ElevatorA(IElevator):
    '''
    Inefficient elevator. Mode A is very inefficient and only allows for
    transporting one person at a time. If Doug calls the elevator on floor 4
    and wants to go to floor 1, and Alan subsequently calls the elevator on
    floor 3 and wants to go to floor 2, the elevator will go pick up Doug and
    take him to floor 1 before picking up Alan and taking him to floor 2.
    '''
    # def __init__(self, *args, **kwargs):
    #     '''
    #     '''

    def run(self, init_flr, flrs_list):
        '''Print to standard output a single line consisting of a
        space-delimited list of floors followed by the total distance in floors
        that the elevator travels, in parenthesis "(" and ")".

        :Example:

            init_flr = 9; flrs_list = [[1, 5], [1, 6], [1, 5]]
            prints out: 9 1 5 1 6 1 5 (30)

        :param init_flr: int. Initial floor
        :param flrs_list: list of list of ints. The floors to traverse.

        :returns: None. Prints out the list of floors and distance.
        '''

        flrs_array = sum(flrs_list, [init_flr])

        dist = 0
        prev_flr = flrs_array[0]
        print prev_flr,

        for flr in flrs_array[1:]:
            if flr == prev_flr:
                # special case
                continue

            dist += abs(flr - prev_flr)
            prev_flr = flr
            print flr,

        print '({})'.format(dist)

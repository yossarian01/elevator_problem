#!/bin/bash

# Regression test script for the elevator problem.

# ref: http://stackoverflow.com/questions/5195607/checking-bash-exit-status-of-several-commands-efficiently

function test {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "error with $1" >&2
    else
        echo "pass"
    fi
    return $status
}

./elevator_cli.py elevator_test_input.txt A > test_outA.txt
echo 'Test Elevator mode A:'
test diff -w outA.txt test_outA.txt
rm test_outA.txt

echo ''

./elevator_cli.py elevator_test_input.txt B > test_outB.txt
echo 'Test Elevator mode B:'
test diff -w outB.txt test_outB.txt
rm test_outB.txt
